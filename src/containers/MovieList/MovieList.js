import React, {Component} from 'react';
import {nanoid} from 'nanoid';
import List from "../../components/List/List";
import './MoviList.css';
import FormList from "../../components/FormList/FormList";

class MovieList extends  Component {
    state = {
        lists: [
            {title: 'Довод', id: nanoid()},
            {title: 'Душа', id: nanoid()},
            {title: 'Чудо-женщина', id: nanoid()}
        ],

        movie: [
            {title: '', id: nanoid()}
        ]
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.addMovie();
    }

    addMovie = (event) => {

    };

    onChangeList = (e, id) => {
        const index = this.state.lists.findIndex(i => i.id === id);
        const lists = [...this.state.lists];
        const list = {...lists[index]};
        list.title = e.target.value;
        lists[index] = list;

        this.setState({lists});
    }

    onChangeHandler = (event) => {
        const movieValue = event.target.value;

        this.setState({movie: movieValue});
    }

    removeList = (id) => {
        const index = this.state.lists.findIndex(i => i.id === id);
        const listsCopy = [...this.state.lists];
        listsCopy.splice(index,1);

        this.setState({lists: listsCopy});
    }

    // shouldComponentUpdate(nextProps, nextState) {
    // }

    render() {


        return (
            <section className="List-section">

                <FormList
                    addForm={event => this.onChangeHandler(event)}
                    task={this.state.movie.title}
                    addMovieToForm={this.addMovie}
                />

                <div className="WatchList">
                    <p>To watch list:</p>
                    {this.state.lists.map(list => (
                        <List
                            key={list.id}
                            title={list.title}
                            changeList={e => this.onChangeList(e, list.id)}
                            remove={() => this.removeList(list.id)}
                        />
                    ))}
                </div>
            </section>
        )
    }
};

export default MovieList;