import React, {useState, useEffect} from 'react';
import {nanoid} from "nanoid";
import Post from "../../components/Post/Post";
import JokePost from "../../components/JokePost/JokePost";

const JokesList = () => {
    const [posts, setPosts] = useState([
        {value: 'Test Post', id: nanoid()},
        {value: 'Next Post', id: nanoid()},
    ]);

    const url = 'https://api.chucknorris.io/jokes/random';

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const post = await response.json();
                const jokeText = post.value;

                const jokesCopy = [...posts];
                jokesCopy.push({value: jokeText, id: nanoid()})

                setPosts(jokesCopy)
            }
        }

        fetchData().catch(console.error);
    }, []);

    return (
        <>
            <section className="Post">
                {posts.map(post => (
                    <Post
                        key={post.id}
                        value={post.value}
                    />
                ))}
            </section>
        </>
    );
};

export default JokesList;