import React from 'react';
import './App.css';
import MovieLIst from './containers/MovieList/MovieList';
import JokesList from "./containers/JokesList/JokesList";

function App() {
  return (
      <div className="App">
          <div className="List-block">
              <MovieLIst/>
          </div>

          <div className="List-block">
              <JokesList/>
          </div>
      </div>
  )
}

export default App;
