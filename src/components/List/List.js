import React, {Component} from 'react';
import './List.css';

class List extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.title !== this.props.title || nextProps.remove !== this.props.remove
    }

    render() {
        return (
            <article className="List">
                <input className="Input-list" type="text" value={this.props.title} onChange={this.props.changeList}/>
                <button className="Button-list" onClick={this.props.remove}>X</button>
            </article>
        )
    }
};

export default List;