import React, {Component} from 'react';

class FormList extends Component {
    render() {
        return (
            <div className="AddList">
                <input className="Input-add" type="text" value={this.props.task} onChange={this.props.addForm} required/>
                <button className="Button-add" onClick={this.props.addMovieToForm}>Add</button>
            </div>
        );
    }
}

export default FormList;