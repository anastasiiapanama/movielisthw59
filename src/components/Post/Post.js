import React from 'react';

const Post = props => {
    return (
        <article className="Post">
            <p>{props.value}</p>
        </article>
    );
};

export default Post;